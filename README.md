# Cudo miner in docker

How to run:

```
docker run -e CUDO_ORG=your-org-name mojo42/cudocker
```

How to build:

```
docker build -t mojo42/cudocker:latest .
```
