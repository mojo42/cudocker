#!/bin/bash

echo "entrypoint.sh $@"

if [ -z "$CUDO_ORG" ]; then
    echo "CUDO_ORG environment variable not set, exiting"
    exit 1
fi

echo "org=${CUDO_ORG}" > /etc/cudo_minerrc
export APP_NAME="Cudo Miner"
/usr/local/cudo-miner/cudo-miner-core
