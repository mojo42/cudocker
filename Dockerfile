FROM debian:11
RUN apt-get update
RUN apt-get install -y curl
RUN curl -o /etc/apt/trusted.gpg.d/cudo.asc https://download.cudo.org/keys/pgp/apt.asc
RUN echo 'deb [arch=amd64] https://download.cudo.org/repo/apt/ stable main' | tee /etc/apt/sources.list.d/cudo.list
RUN apt-get update && apt-get install -y cudo-miner-core

COPY entrypoint.sh .
RUN chmod +x entrypoint.sh
ENTRYPOINT [ "./entrypoint.sh" ]